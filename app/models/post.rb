# encoding: utf-8
class Post < ActiveRecord::Base
  def date
    created_at.strftime("%a %_d, %h %Y")
  end

  def self.publish_years
    all.map { |post| post.created_at.year }.uniq
  end

  def self.publish_months_by_year(year)
    by_year(year).map { |post| post.created_at.month }.uniq
  end

  def self.by_year(year)
    low   = DateTime.new(year, 1)
    high  = DateTime.new((year+1), 1)
    where("created_at < ? AND created_at >= ?", high, low).order("created_at ASC")
  end

  def self.by_year_range(from, to)
    from  = DateTime.new(from)
    to    = DateTime.new(to)
    where("created_at >= ? AND created_at < ?", from, to).order("created_at ASC")
  end

  def self.by_year_month(year, month)
    if month == 12
      year = year+1
      month = 1
    end
    low  = DateTime.new(year, month)
    high = DateTime.new(year, (month+1))
    where("created_at < ? AND created_at >= ?", high, low).order("created_at ASC")
  end
end

