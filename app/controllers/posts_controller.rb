class PostsController < ApplicationController
  def index
    @posts = Post.where("title LIKE ? OR content LIKE ? OR created_at LIKE ?", 
                        "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%")
  end
end
