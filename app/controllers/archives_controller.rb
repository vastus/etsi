class ArchivesController < ApplicationController
  def show
    if params[:year] and not params[:month]
      @posts = Post.by_year(params[:year].to_i)
    elsif params[:year] and params[:month]
      @posts = Post.by_year_month(params[:year].to_i, params[:month].to_i)
    end
  end
end
